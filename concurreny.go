package main

import (
	"fmt"
	"sync"
	"time"
)

type preparation struct {
	m sync.Mutex
}

func getBack(smt string, c chan string) {
	smt = smt + "  "
	c <- smt
}

func printer(n int, c chan string) {

	var full string
	// for i := 0; i <= n; i++ {
	// 	w := <-c
	// 	full += w
	// }

	one, two, three := <-c, <-c, <-c
	full = one + two + three
	fmt.Println(full)
}

func capChan() {
	c := make(chan string, 3)
	go getBack("birinchi", c)
	go getBack("ikkinchi", c)
	go getBack("uchinchi", c)

	go printer(cap(c), c)

	time.Sleep(100 * time.Millisecond)

}

func run(c chan int) {
	c <- 1
}

func race() {
	runner1, runner2 := make(chan int), make(chan int)
	var r1, r2, r3 int
	for i := 1; i <= 1000; i++ {
		go run(runner1)
		go run(runner2)

		select {
		case <-runner1:
			r1++

		case <-runner2:
			r2++

		default:
			r3++
		}
	}

	fmt.Printf("Runner1 is %d, Runner2 is %d, default: %d\n", r1, r2, r3)
}

func (p *preparation) prepare(smt string) {
	p.m.Lock()
	fmt.Println(smt)
	defer p.m.Unlock()
}

func exam() {
	p := preparation{}
	var s string
	go p.prepare("print1")
	go p.prepare("print2")
	go p.prepare("print3")
	time.Sleep(10 * time.Millisecond)
	fmt.Println(s)
}

func main() {
	capChan()
	race()
	exam()
}
